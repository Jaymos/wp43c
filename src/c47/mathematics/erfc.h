// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file erfc.h
 ***********************************************/
#if !defined(ERFC_H)
  #define ERFC_H

  void fnErfc   (uint16_t unusedButMandatoryParameter);

#endif // !ERFC_H
