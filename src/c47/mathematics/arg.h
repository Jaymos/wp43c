// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file arctan.h
 ***********************************************/
#if !defined(ARG_H)
  #define ARG_H

  void fnArg      (uint16_t unusedButMandatoryParameter);

#endif // !ARG_H
