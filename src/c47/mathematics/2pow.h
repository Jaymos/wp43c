// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file 2pow.h
 ***********************************************/
#if !defined(FILE_2POW_H)
  #define FILE_2POW_H

  void fn2Pow     (uint16_t unusedButMandatoryParameter);
  void realPower2 (const real_t *x, real_t *res, realContext_t *realContext);
#endif // !FILE_2POW_H
