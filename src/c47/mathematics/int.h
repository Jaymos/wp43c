// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file int.h
 ***********************************************/
#if !defined(INT_H)
  #define INT_H

  void fnCheckInteger(uint16_t mode);
#endif // !INT_H
