// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file cosh.h
 ***********************************************/
#if !defined(COSH_H)
  #define COSH_H

  void fnCosh   (uint16_t unusedButMandatoryParameter);
#endif // !COSH_H
