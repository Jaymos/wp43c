// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file cxToRe.h
 ***********************************************/
#if !defined(CXTORE_H)
  #define CXTORE_H

  void fnCxToRe(uint16_t unusedButMandatoryParameter);
#endif // !CXTORE_H
