// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file swapRealImaginary.h
 ***********************************************/
#if !defined(SWAPREALIMAGINARY_H)
#define SWAPREALIMAGINARY_H

void fnSwapRealImaginary(uint16_t unusedButMandatoryParameter);

#endif // !SWAPREALIMAGINARY_H
