// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file parallel.h
 ***********************************************/
#if !defined(PARALLEL_H)
  #define PARALLEL_H

  void fnParallel      (uint16_t unusedButMandatoryParameter);
#endif // !PARALLEL_H
