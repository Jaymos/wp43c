// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file min.h
 ***********************************************/
#if !defined(MIN_H)
  #define MIN_H

  void fnMin(uint16_t unusedButMandatoryParameter);
#endif // !MIN_H
