// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file cross.h
 ***********************************************/
#if !defined(CROSS_H)
  #define CROSS_H

  void fnCross(uint16_t unusedButMandatoryParameter);
#endif // !CROSS_H
