// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file remainder.h
 ***********************************************/
#if !defined(REMAINDER_H)
  #define REMAINDER_H

  void fnRmd(uint16_t unusedButMandatoryParameter);

#endif // !REMAINDER_H
