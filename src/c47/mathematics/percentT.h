// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file percentT.h
 ***********************************************/
#if !defined(PERCENTT_H)
  #define PERCENTT_H

  void fnPercentT(uint16_t unusedButMandatoryParameter);

  void percentTLonILonI(void);
  void percentTLonIReal(void);
  void percentTRealLonI(void);
  void percentTRealReal(void);
#endif // !PERCENTT_H
