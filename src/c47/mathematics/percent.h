// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file percent.h
 ***********************************************/
#if !defined(PERCENT_H)
  #define PERCENT_H

  void fnPercent(uint16_t unusedButMandatoryParameter);
#endif // !PERCENT_H
