// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file asnBrowser.h
 ***********************************************/
#if !defined(ASNBROWSER_H)
  #define ASNBROWSER_H

  /********************************************//**
   * \brief The assigned key browser
   *
   * \param[in] unusedButMandatoryParameter uint16_t
   * \return void
   ***********************************************/
  void fnAsnViewer(uint16_t unusedButMandatoryParameter);
#endif // !REGISTERBROWSER_H
