// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file registerBrowser.h
 ***********************************************/
#if !defined(REGISTERBROWSER_H)
  #define REGISTERBROWSER_H

  /********************************************//**
   * \brief The register browser
   *
   * \param[in] unusedButMandatoryParameter uint16_t
   * \return void
   ***********************************************/
  void registerBrowser(uint16_t unusedButMandatoryParameter);
#endif // !REGISTERBROWSER_H
