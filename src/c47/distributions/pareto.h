// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The C47 Authors

/********************************************//**
 * \file pareto.h
 ***********************************************/

#if !defined(PARETO_H)
  #define PARETO_H

  void fnParetoP   (uint16_t unusedButMandatoryParameter);
  void fnParetoL   (uint16_t unusedButMandatoryParameter);
  void fnParetoU   (uint16_t unusedButMandatoryParameter);
  void fnParetoI   (uint16_t unusedButMandatoryParameter);

  void fnPareto2P  (uint16_t unusedButMandatoryParameter);
  void fnPareto2L  (uint16_t unusedButMandatoryParameter);
  void fnPareto2U  (uint16_t unusedButMandatoryParameter);
  void fnPareto2I  (uint16_t unusedButMandatoryParameter);
#endif // !PARETO_H
