// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file nand.h
 ***********************************************/
#if !defined(NAND_H)
  #define NAND_H

  void fnLogicalNand(uint16_t unusedButMandatoryParameter);
#endif // !NAND_H
