// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file xnor.h
 ***********************************************/
#if !defined(XNOR_H)
  #define XNOR_H

  void fnLogicalXnor(uint16_t unusedButMandatoryParameter);
#endif // !XNOR_H
