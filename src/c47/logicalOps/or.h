// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file or.h
 ***********************************************/
#if !defined(OR_H)
  #define OR_H

  void fnLogicalOr(uint16_t unusedButMandatoryParameter);
#endif // !OR_H
