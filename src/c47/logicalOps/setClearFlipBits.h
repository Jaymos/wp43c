// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file setClearFlipBits.h
 ***********************************************/
#if !defined(SETCLEARFLIPBITS_H)
  #define SETCLEARFLIPBITS_H

  void fnCb(uint16_t bit);
  void fnSb(uint16_t bit);
  void fnFb(uint16_t bit);
  void fnBc(uint16_t bit);
  void fnBs(uint16_t bit);
#endif // !SETCLEARFLIPBITS_H
